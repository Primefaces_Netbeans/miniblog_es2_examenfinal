package com.mitocode.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;


@Named
@ViewScoped
@ManagedBean
public class UsuarioBean implements Serializable {

	@Inject
	private IUsuarioService service;
	

	private Usuario usuario; 
	private List<Usuario> lista;
	private String tipoDialog;
	private String nombre_usuario;
	private String valor ="1" ;
	private String contrasena="" ; 
	


	@PostConstruct
	public void init() {

		this.usuario = new Usuario();
		this.listar();
		this.tipoDialog = "Nuevo";
		this.getValor();

	}
	
	
	public boolean validarcontrasena() {
  
		boolean activo=true;
		
		try {
			
			 
			String contr;  
			contr= this.getContrasena();
			
			
			this.usuario.setContrasena(contr);
			Usuario user = service.validarconstrasena(usuario); 
			if(user != null && user.getContrasena() != null ) {
				//Almacenar en la sesion de JSF y proseguir con la navegacion
				
				this.valor = "0";
				activo =false;
				this.setContrasena("");
				
			}else {
				this.valor = "1"; 
				activo = true;
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales incorrectas"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", e.getMessage()));
		}

		return activo;

	}
	

	public void listarBusquedaUsuario() {
		try {
			 
			String us;
			us= this.getNombre_usuario();
			
			this.usuario.setUsuario(us);  
			
			lista = service.listarporusuario(usuario);
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} 
	} 
	
	

	public void operar(String accion) {
		try {

			if (accion.equalsIgnoreCase("R")) {
				this.service.registrar(this.usuario);
			} else if (accion.equalsIgnoreCase("M")) {
				
				String clave = this.usuario.getContrasena();
				String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt()); 
				this.usuario.setContrasena(claveHash);
				
				this.service.modificar(this.usuario);
				
				
				
			}
			
			this.listar();
			this.getValor();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listar() {
		try {
			
			
			
			this.lista = this.service.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
 
	public void mostrarData(Usuario p) {
		
		this.setValor("1");
		this.usuario = p;
		this.tipoDialog = "Modificar";
		
	}
	
	public void limpiarControles() {
		this.usuario = new Usuario();
		this.tipoDialog = "Nuevo";
	}


	/**
	 * 
	 * getters & setters
	 */

	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	
	public String getTipoDialog() {
		return tipoDialog;
	}

	public void setTipoDialog(String tipoDialog) {
		this.tipoDialog = tipoDialog;
	}

	public String getNombre_usuario() {
		return nombre_usuario ; 
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}


	public String getValor() {
		return valor;
	}


	public void setValor(String valor) {
		this.valor = valor;
	}


	public String getContrasena() {
		return contrasena;
	}


	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


	

	 

}
