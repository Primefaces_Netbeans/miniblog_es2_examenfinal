package com.mitocode.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable {

	@EJB
	private IUsuarioDAO dao;

	@Override
	public Usuario login(Usuario us) {
		String clave = us.getContrasena();
		String claveHash = dao.traerPassHashed(us.getUsuario());

		try {
			if (BCrypt.checkpw(clave, claveHash)) {
				return dao.leerPorNombreUsuario(us.getUsuario());
			} else {
				
				 System.out.println("It does not match"); 
			}
			
		} catch (Exception e) {
			throw e;
		}

		return new Usuario();
	}

	@Override
	public Integer registrar(Usuario t) throws Exception {
		
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		
		return dao.eliminar(t);
	}

	@Override
	public List<Usuario> listar() throws Exception {
		
		return dao.listar();
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
	
		return dao.listarPorId(t);
	}

	@Override
	public List<Usuario> listarporusuario(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return dao.listarporusuario(t);
	}

	@Override
	public Usuario validarconstrasena(Usuario us) {
		
		String clave1 = us.getContrasena();
		String claveHash1 = dao.traerPassHashed(us.getUsuario());
		
		try {
			
			if (BCrypt.checkpw(clave1, claveHash1)) {
			  
			
				return dao.leerPorNombreUsuario(us.getUsuario());
			}  else {
				
				 System.out.println("It does not match");
			}
				
		} catch (Exception e) {
			throw e;
		}

		return new Usuario();
	  
	}
	
	

}
